﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyHP : MonoBehaviour {

	public int startingHealth = 5;            
	public int currentHealth;                  
	public float sinkSpeed = 2.5f;                           


	//Animator anim;                           
	//AudioSource enemyAudio;                    
	//ParticleSystem hitParticles;                
	CapsuleCollider capsuleCollider;            
	bool isDead;                                
	bool isSinking;                             


	void Awake ()
	{
		//anim = GetComponent <Animator> ();
		//enemyAudio = GetComponent <AudioSource> ();
		//hitParticles = GetComponentInChildren <ParticleSystem> ();
		capsuleCollider = GetComponent <CapsuleCollider> ();

		currentHealth = startingHealth;
	}

	void Update ()
	{
		if(isSinking)
		{
			transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
		}
	}


	public void TakeDamage (int amount) //Vector3 hitPoint)
	{
		//if(isDead)
		//	return;

		// Play the hurt sound effect.
		//enemyAudio.Play ();

		// Reduce the current health by the amount of damage sustained.
		currentHealth -= amount;

		// Set the position of the particle system to where the hit was sustained.
		//hitParticles.transform.position = hitPoint;

		// And play the particles.
		//hitParticles.Play();

		if(currentHealth <= 0)
		{
			Death ();
		}
	}


	void Death ()
	{
		// The enemy is dead.
		isDead = true;

		//capsuleCollider.isTrigger = true;
		gameObject.SetActive (false);
		//anim.SetTrigger ("Dead");

		//enemyAudio.clip = deathClip;
		//enemyAudio.Play ();
	}


	public void StartSinking ()
	{
		// Find and disable the Nav Mesh Agent.
		GetComponent <NavMeshAgent> ().enabled = false;

		// Find the rigidbody component and make it kinematic (since we use Translate to sink the enemy).
		GetComponent <Rigidbody> ().isKinematic = true;

		// The enemy should no sink.
		isSinking = true;

		// After 2 seconds destory the enemy.
		//Destroy (gameObject, 2f);
	}
}
