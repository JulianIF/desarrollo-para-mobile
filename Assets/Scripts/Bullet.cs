﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour 
{
	[SerializeField] private int tiempoDeVida;
	[SerializeField] private float bulletSpeed;
	private Rigidbody rb;
	int dmg = 1;

	// Use this for initialization
	void OnEnable () 
	{
		gameObject.SetActive (true);
		Invoke ("Destruir", tiempoDeVida);
		//rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		Move ();
	}

	private void Move ()
	{
		transform.position += transform.forward * Time.deltaTime * bulletSpeed;
	}

	private void OnTriggerEnter (Collider other)
	{
		Debug.Log ("entro trigger");
		if (other.CompareTag ("Enemy")) 
		{
			other.GetComponent<EnemyHP> ().TakeDamage(dmg);
			//Destroy (other.gameObject);
			Destruir ();
		}
		if (other.CompareTag ("Walls"))
			Destruir ();

	}

	public void Destruir ()
	{
		gameObject.SetActive (false);
	}
}
