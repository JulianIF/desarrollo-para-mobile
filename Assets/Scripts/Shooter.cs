﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {

	public ObjectPooler objectPooler;
	// Use this for initialization
	void Start () 
	{
		objectPooler = this.GetComponent<ObjectPooler> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetMouseButtonDown(0)) 
		{
			Shoot ();
		}
	}

	private void Shoot ()
	{	
		GameObject bullet = objectPooler.GetPooledObject();
		if (bullet != null) 
		{
			bullet.transform.position = this.transform.position;
			bullet.transform.rotation = this.transform.rotation;
			bullet.SetActive(true);
		}
	}
}
