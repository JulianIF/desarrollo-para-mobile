﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {

	public float spawnTime;
	public ObjectPooler objectPooler;
	public int enemiesSpawned;

	void Start () 
	{
		objectPooler = this.GetComponent<ObjectPooler> ();
		InvokeRepeating ("Spawn", spawnTime, spawnTime);
	}

	void Update () 
	{
		
	}

	private void Spawn()
	{
		if (enemiesSpawned <= objectPooler.ammountToPool) 
		{
			GameObject enemy = objectPooler.GetPooledObject ();
			if (enemy != null && enemy.GetComponent<EnemyHP>().currentHealth !=0) 
			{
				enemy.transform.position = this.transform.position;
				enemy.transform.rotation = this.transform.rotation;
				enemy.SetActive (true);
			}
			enemiesSpawned++;
		} 
		else
			return;
	}
}
