﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossedDoor : MonoBehaviour 
{
	public EnemySpawn nextSpawn;
	public GameObject player;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerExit (Collider other)
	{
		if(other.gameObject == player)
		{
			nextSpawn.gameObject.SetActive (true);
		}
	}
}
