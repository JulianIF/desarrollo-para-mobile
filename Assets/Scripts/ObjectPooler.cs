﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//raywenderlich object pooling unity
public class ObjectPooler : MonoBehaviour {

	public static ObjectPooler SharedInstance;
	public List <GameObject> pooledObjects;
	public GameObject objectToPool;
	public int ammountToPool;
	// Use this for initialization
	void Start () 
	{
		pooledObjects=new List<GameObject>();
		for (int i = 0; i < ammountToPool; i++) 
		{
			GameObject b = (GameObject)Instantiate (objectToPool);
			b.SetActive (false);
			pooledObjects.Add (b);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Awake ()
	{

	}

	public GameObject GetPooledObject()
	{
		for (int i = 0; i < pooledObjects.Count; i++) 
		{
			if (!pooledObjects [i].activeInHierarchy) 
			{
				if (!pooledObjects [i].CompareTag ("Enemy")) 
				{
					return pooledObjects [i];
				} 
				else if (pooledObjects[i].GetComponent<EnemyHP>().currentHealth > 0)
				{
					return pooledObjects [i];
				}
			}
		}
		return null;
	}

	public bool CheckEmpty()
	{
		for (int i = 0; i < pooledObjects.Count; i++) 
		{
			if (pooledObjects[i].activeInHierarchy)
				return false;
		}
		return true;
	}
		
}
